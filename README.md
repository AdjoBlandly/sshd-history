# SSH Analysis

This is a long-term slow-scan of the SSH servers exposed on the
internet.

It started in 2014, and is composed of 10M+ hosts. It chooses host
randomly, at the current pace it hit each host ~yearly (randomly...).


## How?

It's a simple crontab line I dropped back then and almost forgot:

```text
* * * * * run-one nmap --dns-servers 9.9.9.9,8.8.8.8 --host-timeout 10m -p22 --open -sV -Pn -iR 4096 -T aggressive --append-output -oG /data/ssh-scans/ssh-$(date +'\%Y-\%m-\%d') >/dev/null
```


## Is it even legal?

See the `Legal issues <https://nmap.org/book/legal-issues.html>`_ page on nmap.org...

TL;DR: It should not, which does not mean it is everywhere.

According to my lawyer, as long as my intention is only to do stats it's OK.

According to my ISP I obviously have a virus on one of my laptops and
should install an anti-virus... ;)


## Why?

The goal is to analyze the deployment rate of new versions, and
incidentally the market share of different implementations.


## OOKKKK show me the data now!1!!


### OpenSSH versions

![](openssh.png)

### Platforms

![](platforms.png)


# Contributing

Have an idea for a missing graph?

You should first run:

    nmap_to_sqlite3.py 'ssh-*'

then play with the Jupyter Notebook:

    jupyter notebook sshd.ipynb

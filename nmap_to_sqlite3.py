"""Parses nmap results, and store them in sqlite3."""

from pathlib import Path
from dataclasses import dataclass
from datetime import datetime
import sqlite3
from functools import lru_cache


class NmapLine:
    """Parses and represents a single line of an nmap file."""

    def __init__(self, line):
        self.kind = None
        self.date = None
        self.tokens = None
        self.port = None
        self.state = None
        self.protocol = None
        self.service = None
        self.version = None
        self.host = None
        self.hostname = None
        if line.startswith("# Nmap"):
            self.parse_comment_line(line)
        elif line.startswith("Host: "):
            self.parse_result_line(line)
        else:
            raise ValueError("Unknown nmap line: {}".format(repr(line)))

    def store(self, conn):
        """Store given line to sqlite3 connection."""
        conn.execute(
            """
INSERT INTO scan VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)""",
            (
                self.date,
                self.host,
                self.hostname,
                self.port,
                self.state,
                self.protocol,
                self.service,
                self.version,
                self.version_name,
                self.version_float,
                self.platform,
            ),
        )

    @property
    def version_float(self):
        """Find more using:
        sqlite> SELECT COUNT(1), version FROM scan WHERE version_float IS NULL AND version != '' GROUP BY version ORDER BY COUNT(1) DESC LIMIT 100;
        """
        for token in self.version.split():
            if all(char in "0123456789.p" for char in token):
                return token

    @property
    def version_name(self):
        for candidate in (
            "OpenSSH",
            "Dropbear",
            "Cisco",
            "RomSShell",
            "FortiSSH",
            "ProFTPD",
        ):
            if candidate.lower() in self.version.lower():
                return candidate

    @property
    def platform(self):
        """Find more using:
        sqlite> SELECT COUNT(1), version FROM scan WHERE platform IS NULL AND version != '' GROUP BY version ORDER BY COUNT(1) DESC LIMIT 100;
        """
        for candidate in (
            "Ubuntu",
            "Debian",
            "MikroTik",
            "Linksys",
            "Cisco",
            "FreeBSD",
            "Huawei",
        ):
            if candidate in self.version:
                return candidate

    def parse_comment_line(self, line):
        """Parses lines like:
        # Nmap 6.46 scan initiated Sat Dec  6 00:00:01 2014 as: \
          nmap --host-timeout 10m -p22 --open -sV -Pn -iR 4096 \
           -T aggressive --append-output -oG /root/ssh-2014-12-06

        or

        # Nmap done at Sun Nov 30 22:02:26 2014 -- 4096 IP addresses…
        """
        self.kind = "comment"
        tokens = line.split()
        if tokens[2] == "done":
            str_date = " ".join(tokens[4:9])
        else:
            str_date = " ".join(tokens[5:10])
        self.date = datetime.strptime(str_date, "%c")
        self.tokens = tokens

    def parse_result_line(self, line):
        """Parses lines like:
        Host: 87.248.202.165 (cds514.ams.llnw.net)    \
          Status: Up

        or

        Host: 87.248.202.165 (cds514.ams.llnw.net)    \
          Ports: 22/open/tcp//tcpwrapped///
        """
        _host, ip, hostname, status_or_ports, details = line.split(maxsplit=4)
        assert _host == "Host:"
        if status_or_ports == "Ports:":
            (
                self.port,
                self.state,
                self.protocol,
                _,
                self.service,
                __,
                self.version,
                ___,
            ) = details.split("/")
            assert _ == __ == ""
            assert ___ == "\n"
        elif status_or_ports == "Status:":
            assert details in ("Up\n", "Timeout\n"), repr(details)
        else:
            raise ValueError(line)
        self.host = ip
        self.hostname = hostname[1:-1]
        self.kind = status_or_ports[:-1]

    def __repr__(self):
        return "<NmapResultLine {} {} {}>".format(self.kind, self.host, self.hostname)


def nmap_to_sqlite3(data_files, sqlite3_path):
    """Parses all nmap files in a given data directory, save the parsed
    content to an sqlite3 file.
    """
    conn = sqlite3.connect(sqlite3_path)
    with conn:
        conn.execute(
            "CREATE TABLE IF NOT EXISTS scan (date, host, hostname, port, state, protocol, service, version, version_name, version_float, platform)"
        )
    date = None
    for nmap_filename in sorted(data_files):
        with conn:
            with open(nmap_filename, encoding="UTF-8") as nmap_file:
                for line in nmap_file:
                    try:
                        line = NmapLine(line)
                    except ValueError as err:
                        print(err)
                        continue
                    if line.kind == "comment":
                        date = line.date
                        continue
                    if line.kind == "Status":
                        continue
                    line.date = date
                    line.store(conn)


if __name__ == "__main__":
    import sys

    nmap_to_sqlite3(Path("data/").glob(sys.argv[1]), "sshd.sqlite3")
